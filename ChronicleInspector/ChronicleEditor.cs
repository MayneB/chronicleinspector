﻿//#define DEBUG_INTERNAL

using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Reflection;
using uObject = UnityEngine.Object;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Resources;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Text;
using ChronicleInspector;
using UnityEditor.AnimatedValues;
using System.Diagnostics;

[InitializeOnLoad]
public class ChronicleEditor : EditorWindow, IHasCustomMenu
{
  static ChronicleEditor()
  {
    if(!EditorPrefs.HasKey("ChronicleEditor:DocsShown"))
    {
      EditorPrefs.SetBool("ChronicleEditor:DocsShown", true);

      ShowDocumentation();
    }
  }

  //Reflection names of classes and functions
  private const string DEFAULT_INSPECTOR_ASSEMBLY_QUALIFIED_NAME = "UnityEditor.InspectorWindow";
  private const string ON_GUI_FUNCTION_NAME = "OnGUI";
  private const string SET_MDOE_FUNCTION_NAME = "SetMode";
  private const string LOCKED_PROPERTY_NAME = "isLocked";

  //GUIContent to give users nice tool tips and labels. 
  private static GUIContent HistoryContent = new GUIContent("Settings", "This will show the settings menu for you to choose stuff from.");
  private static GUIContent BackArrowContent = new GUIContent("<<", "This will take you back to your last selection");
  private static GUIContent ForwardArrowContent = new GUIContent(">>", "This will take you to your forward");

  //References to our base inspector
  private EditorWindow _baseInspector;
  private MethodInfo _OnGUIMethod;
  private MethodInfo _SetModeMethod;
  private PropertyInfo _lockedProperty;
  private InspectorMode _mode = InspectorMode.Normal;

  //The locking options 
  private GUIStyle _lockButtonStyle;
  private bool _isLocked = false;

  //Our animated values to make the inspector look and feel nice. 
  private AnimBool m_ShowBreadCrumbs = new AnimBool();
  private AnimBool m_ShowSettings = new AnimBool();

  //Settings
  private int m_BreadcrumbCount = 4;
  private int m_HistoryCount = 10;

  private GUIContent m_BreadcrumbCountContent = new GUIContent("Breadcrumb Count", "This is how many elements in each direction the breadcrumb will try to draw.");
  private GUIContent m_HistoyCountContent = new GUIContent("History Size", "This is how much history Chronicle will store before overwriting old data.");

  //Editor Pref keys
  private const string SHOW_BREAD_CRUMBS_KEY = "ChronicleEditor:m_ShowBreadCrumbs";
  private const string SHOW_SETTINGS_KEY = "ChronicleEditor:m_ShowSettings";
  private const string HISTORY_COUNT_KEY = "ChronicleEditor:m_HistoryCount";
  private const string BREAD_CRUMB_COUNT_KEY = "ChronicleEditor:m_BreadcurmbCount";

  /// <summary>
  /// This is our menu item which will create our instance. 
  /// </summary>
  [MenuItem("Window/Chronicle")]
  public static void GetWindow()
  {
    EditorWindow.GetWindow<ChronicleEditor>();
  }

  /// <summary>
  /// This is what will init our inspector.
  /// </summary>
  public void OnEnable()
  {
    ClearValues();

    ChronicleWatcher.CreateOrGetInstance();

    m_ShowBreadCrumbs.value = false;
    m_ShowSettings.value = false;

    m_ShowBreadCrumbs.valueChanged.AddListener(Repaint);
    m_ShowSettings.valueChanged.AddListener(Repaint);

    LoadInsepctor();

    ChronicleWatcher.instance.Setup();

    EditorApplication.update += ChronicleWatcher.instance.Update;

    LoadPrefs();

    this.Repaint();
  }

  /// <summary>
  /// This function is used to clearn up the inspector
  /// it also marks our classes to be saved. 
  /// </summary>
  public void OnDisable()
  {
    UnityEngine.Debug.Log("SAVING FILE");
    SavePrefs();

    ClearValues();

    m_ShowBreadCrumbs.valueChanged.RemoveListener(Repaint);
    m_ShowSettings.valueChanged.RemoveListener(Repaint);

    ChronicleWatcher.Save();
  }

  #region -= Preferences =-
  /// <summary>
  /// Here is where we load all our preferences for the inspector. 
  /// </summary>
  private void LoadPrefs()
  {
    m_ShowBreadCrumbs.target = EditorPrefs.GetBool(SHOW_BREAD_CRUMBS_KEY, false);
    m_ShowSettings.target = EditorPrefs.GetBool(SHOW_SETTINGS_KEY, false);

    m_ShowBreadCrumbs.value = m_ShowBreadCrumbs.target;
    m_ShowSettings.value = m_ShowSettings.target;

    m_HistoryCount = EditorPrefs.GetInt(HISTORY_COUNT_KEY, 15);
    m_BreadcrumbCount = EditorPrefs.GetInt(BREAD_CRUMB_COUNT_KEY, 4);
  }

  /// <summary>
  /// Before the editor is shut down we save all our prefereces to 
  /// editor prefs. 
  /// </summary>
  private void SavePrefs()
  {
    EditorPrefs.SetBool(SHOW_BREAD_CRUMBS_KEY, m_ShowBreadCrumbs.value);
    EditorPrefs.SetBool(SHOW_SETTINGS_KEY, m_ShowSettings.value);

    EditorPrefs.SetInt(HISTORY_COUNT_KEY, m_HistoryCount);
    EditorPrefs.SetInt(BREAD_CRUMB_COUNT_KEY, m_BreadcrumbCount);
  }
  #endregion 

  /// <summary>
  /// Incase anything goes wrong we use this function
  /// to revert all the values we loaded from our assembly.
  /// This should really only be called in OnDisable/OnEnable but
  /// you never know since our code is wrapped in a try catch. 
  /// </summary>
  private void ClearValues()
  {
    if (_baseInspector != null)
    {
      EditorWindow.DestroyImmediate(_baseInspector);
    }

    _OnGUIMethod = null;
    _baseInspector = null;
    _lockedProperty = null;
  }

  /// <summary>
  /// This is used for the visual toggle of the lock
  /// at the top of the inspector.
  /// </summary>
  private bool isLocked
  {
    get
    {
      return _isLocked;
    }
    set
    {
      if (_isLocked == value)
      {
        return;
      }

      FlipLock();
    }
  }

  /// <summary>
  /// This is how we grab the default inspector. This is somewhat "hacky" but 
  /// since Unity will never change the following function names we know this will
  /// work for a long time. This is the best way I could come up with to do this. I
  /// also tried to use System.Reflection.Emit but it blew up in a clould of flames. 
  /// </summary>
  private void LoadInsepctor()
  {
    //Get our assembly
    Assembly editorAssembly = Assembly.GetAssembly(typeof(Editor));
    //Get our base type
    Type inspectorType = editorAssembly.GetType(DEFAULT_INSPECTOR_ASSEMBLY_QUALIFIED_NAME, false, false);
    //Get the OnGUI Method
    _OnGUIMethod = inspectorType.GetMethod(ON_GUI_FUNCTION_NAME, BindingFlags.NonPublic | BindingFlags.Instance);
    //Our Method Mode Setter
    _SetModeMethod = inspectorType.GetMethod(SET_MDOE_FUNCTION_NAME, BindingFlags.NonPublic | BindingFlags.Instance);
    //This is so we can lock the inspector on an object.
    _lockedProperty = inspectorType.GetProperty(LOCKED_PROPERTY_NAME, BindingFlags.Public | BindingFlags.Instance);
    //Create an instance so we can draw it.
    _baseInspector = EditorWindow.CreateInstance(inspectorType) as EditorWindow;
  }

  /// <summary>
  /// This is the deafult gui for Chronicle. It draws the ui for
  /// all our buttons and controls. It has no control over the data
  /// itself as that is all done in ChronicleWatcher. 
  /// </summary>
  public void DrawHistroyHeader()
  {
    EditorGUILayout.BeginHorizontal();
    {
      EditorGUI.BeginDisabledGroup(!ChronicleWatcher.instance.hasPrevious);
      {
        if (GUILayout.Button(BackArrowContent, EditorStyles.toolbarButton))
        {
          ChronicleWatcher.instance.MovePrevious();
        }
      }
      EditorGUI.EndDisabledGroup();

      Rect menuRect = GUILayoutUtility.GetRect(HistoryContent, EditorStyles.toolbarButton);

      m_ShowSettings.target = GUI.Toggle(menuRect, m_ShowSettings.target, HistoryContent, EditorStyles.toolbarButton);


      EditorGUI.BeginDisabledGroup(!ChronicleWatcher.instance.hasNext);
      {
        if (GUILayout.Button(ForwardArrowContent, EditorStyles.toolbarButton))
        {
          ChronicleWatcher.instance.MoveNext();
        }
      }
      EditorGUI.EndDisabledGroup();
    }
    EditorGUILayout.EndHorizontal();

    if (EditorGUILayout.BeginFadeGroup(m_ShowSettings.faded))
    {
      DrawSettings();
    }
    EditorGUILayout.EndFadeGroup();

    if (EditorGUILayout.BeginFadeGroup(m_ShowBreadCrumbs.faded))
    {
      DrawBreadCrumb();
    }
    EditorGUILayout.EndFadeGroup();
  }

  /// <summary>
  /// This will draw any extra settings we want
  /// to show up. This allows the user to easly edit
  /// any values without leaving the current window. 
  /// </summary>
  private void DrawSettings()
  {
    EditorGUI.BeginChangeCheck();
    {
      //This stops the style from expanding to much. //AnimationCurveEditorBackground
      GUILayout.BeginVertical("PopupCurveSwatchBackground", GUILayout.MinHeight(0f), GUILayout.ExpandHeight(false));
      {
        m_ShowBreadCrumbs.target = EditorGUILayout.Toggle("Show Breadcrumbs", m_ShowBreadCrumbs.target);

        if (EditorGUILayout.BeginFadeGroup(m_ShowBreadCrumbs.faded))
        {
          m_BreadcrumbCount = EditorGUILayout.IntField(m_BreadcrumbCountContent, m_BreadcrumbCount);

          if (m_BreadcrumbCount < 0)
          {
            m_BreadcrumbCount = 0;
          }
        }
        EditorGUILayout.EndFadeGroup();

       // m_HistoryCount = EditorGUILayout.IntField(m_HistoyCountContent, m_HistoryCount);
      }
      GUILayout.EndVertical();
    }
    if (EditorGUI.EndChangeCheck())
    {
      SavePrefs();
    }
  }

  /// <summary>
  /// This is the bread crumb trail. This allows the user
  /// to see the most recent history they have had. In the current
  /// state a lot of these options should be pushed into settings
  /// instead of being hard coded. 
  /// </summary>
  private void DrawBreadCrumb()
  {
    GUILayout.BeginHorizontal(EmbeddedResources.breadcrumbFoldoutStyle, GUILayout.Height(40), GUILayout.ExpandWidth(true));
    {
      GUILayout.FlexibleSpace();
      float elementWidth = 80;
      float elementCount = 0;
      float workingWidth = EditorGUIUtility.currentViewWidth - 80f;

      elementCount = workingWidth / elementWidth;

      float count = 0;
      
      foreach (HistoryObject obj in ChronicleWatcher.instance.GetFocusedHistory(m_BreadcrumbCount))
      {
        if (obj == null)
        {
          continue;
        }

        if(obj.hObject == null)
        {
          //An object is trying to draw a null entry. We need to clean up. 
          continue;
        }

        string name = obj.name;

        if (obj.isPrimary)
        {
          Rect currentRect = GUILayoutUtility.GetRect(new GUIContent(name), EmbeddedResources.breadcrumb_primaryStyle, GUILayout.MinWidth(50), GUILayout.ExpandWidth(false));
          GUI.Box(currentRect, new GUIContent(name), EmbeddedResources.breadcrumb_primaryStyle);
          {
            if(Event.current.type == EventType.MouseDrag && currentRect.Contains(Event.current.mousePosition))
            {
               DragAndDrop.StartDrag("Chronicle Drag");
               DragAndDrop.objectReferences = new uObject[] { obj.hObject };
               DragAndDrop.visualMode = DragAndDropVisualMode.Link;
            }
          }
        }
        else
        {
          Rect currentRect = GUILayoutUtility.GetRect(new GUIContent(name), EmbeddedResources.breadcrumb_boxStyle, GUILayout.MinWidth(50), GUILayout.ExpandWidth(false));

          GUI.Box(currentRect, new GUIContent(name), EmbeddedResources.breadcrumb_boxStyle);
          {
            if(currentRect.Contains(Event.current.mousePosition))
            {
              if(Event.current.type == EventType.MouseDrag)
              {
                DragAndDrop.StartDrag("Chronicle Drag");
                DragAndDrop.objectReferences = new uObject[] { obj.hObject };
                DragAndDrop.visualMode = DragAndDropVisualMode.Link;
              }

              if (Event.current.type == EventType.MouseUp)
              {
                ChronicleWatcher.instance.OnElementClicked(obj);
              }
            }
          }
        }


        Rect lastRect = GUILayoutUtility.GetLastRect();

        if (lastRect.x + lastRect.width > Screen.width - 10)
        {
          break;
        }
        count++;
      }

      GUILayout.FlexibleSpace();

    }
    GUILayout.EndHorizontal();
  }

  /// <summary>
  /// This is the entry point to our drawing. It first makes sure our
  /// reflection values are set up. If they are it will draw Unity's default
  /// inspector but before that It will start drawing Chronicle. 
  /// </summary>
  public void OnGUI()
  {
    DrawHistroyHeader();

    if (_lockedProperty == null || _baseInspector == null || _lockedProperty == null)
    {
      LoadInsepctor();

      GUILayout.Label("Failed to find base inspector.", EditorStyles.wordWrappedMiniLabel);
    }
    else
    {
      _OnGUIMethod.Invoke(_baseInspector, null);
    }

    this.Repaint();
  }

  private void ShowButton(Rect position)
  {
    if (_lockButtonStyle == null)
    {
      _lockButtonStyle = "IN LockButton";
    }

    isLocked = GUI.Toggle(position, isLocked, GUIContent.none, _lockButtonStyle);
  }

  /// <summary>
  /// This function is called when we change our inspector mode.
  /// This is just a handy function for our Context Menu Dropdown. 
  /// </summary>
  private void ChangeInspectorMode(InspectorMode mode)
  {
    _mode = mode;

    if (_SetModeMethod == null || _baseInspector == null)
    {
      LoadInsepctor();
    }

    try
    {
      _SetModeMethod.Invoke(_baseInspector, new object[] { _mode });
    }
    catch
    {
      //Something broke so we are going to try to reset it. 
      ClearValues();
    }
  }

  /// <summary>
  /// This is used to Lock the inspector just like the
  /// Unity default one. 
  /// </summary>
  private void FlipLock()
  {
    _isLocked = !_isLocked;

    if (_lockedProperty == null || _baseInspector == null || _lockedProperty == null)
    {
      LoadInsepctor();
    }
    else
    {
      _lockedProperty.SetValue(_baseInspector, _isLocked, null);
    }

  }



  private static void ShowDocumentation()
  {
    Process.Start("https://docs.google.com/document/d/1rlbdijMDhYf4ilhn7LvQHLQLXJ605GhSceQPA6wNtw8/edit?usp=sharing"); 
  }

  /// <summary>
  /// This creates the right click menu options for our context menu. It's pretty
  /// much a copy paste of Unitys with a few added functions. 
  /// </summary>
  public void AddItemsToMenu(GenericMenu menu)
  {
    menu.AddItem(new GUIContent("Normal"), _mode == InspectorMode.Normal, () => { ChangeInspectorMode(InspectorMode.Normal); });
    menu.AddItem(new GUIContent("Debug"), _mode == InspectorMode.Debug, () => { ChangeInspectorMode(InspectorMode.Debug); });
    menu.AddItem(new GUIContent("Debug Internal"), _mode == InspectorMode.DebugInternal, () => { ChangeInspectorMode(InspectorMode.DebugInternal); });
    menu.AddSeparator("");
    menu.AddItem(new GUIContent("Breadcrumb"), m_ShowBreadCrumbs.target, () => { m_ShowBreadCrumbs.target = !m_ShowBreadCrumbs.target; });
    menu.AddItem(new GUIContent("Reset History"), false, ChronicleWatcher.instance.Reset);
    menu.AddSeparator("");
    menu.AddItem(new GUIContent("Lock"), _isLocked, FlipLock);
    menu.AddSeparator("");
    menu.AddItem(new GUIContent("Documentation"), false, ShowDocumentation);

#if DEBUG_INTERNAL
    menu.AddSeparator("");
    menu.AddItem(new GUIContent("Clear Save data"), _isLocked, () => { EditorPrefs.DeleteKey(ChronicleWatcher.EDITOR_PREF_XML_KEY); });
#endif
  }
}