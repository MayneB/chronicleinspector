﻿using System.IO;
using UnityEditor;
using UnityEngine;
using uObject = UnityEngine.Object;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;
using ChronicleInspector.Interfaces;

namespace ChronicleInspector
{
  [DataContract(Name = DataContractConstants.XML_NAME_WATCHER)]
  public class ChronicleWatcher : ISave
  {
    public const string EDITOR_PREF_XML_KEY = "ChroncileWatcher:XML:";

    private static ChronicleWatcher _instance;

    public static ChronicleWatcher instance
    {
      get
      {
        if (_instance == null)
        {
          _instance = CreateOrGetInstance();
        }
        return _instance;
      }
    }

    public static ChronicleWatcher CreateOrGetInstance()
    {
      if (_instance != null)
      {
        return _instance;
      }

      string xmlData = EditorPrefs.GetString(EDITOR_PREF_XML_KEY, null);

      if (!string.IsNullOrEmpty(xmlData))
      {
        using (StringReader reader = new StringReader(xmlData))
        {
          using (XmlReader xml = XmlReader.Create(reader))
          {
            DataContractSerializer serialzer = new DataContractSerializer(typeof(ChronicleWatcher));
            _instance = (ChronicleWatcher)serialzer.ReadObject(xml);

            if(_instance._timeline == null)
            {
              _instance._timeline = new HistoryTimelime();
            }

            _instance.AfterLoad();
          }
        }
      }
      else
      {
        _instance = System.Activator.CreateInstance<ChronicleWatcher>();
      }

      return _instance;
    }

    public static void Save()
    {
      if (_instance != null)
      {
        using (StringWriter writer = new StringWriter())
        {
          using (XmlWriter xml = XmlWriter.Create(writer))
          {
            _instance.BeforeSave();
            DataContractSerializer serialzer = new DataContractSerializer(typeof(ChronicleWatcher));
            serialzer.WriteObject(xml, _instance);
            EditorPrefs.SetString(EDITOR_PREF_XML_KEY, writer.ToString());
          }
        }
      }
    }

    public void Reset()
    {
      _timeline.Reset();
    }

    private uObject _lastSelection;

    [DataMember(Name = DataContractConstants.XML_NAME_HISTORY_TIMELINE, EmitDefaultValue=true)]
    private HistoryTimelime _timeline = new HistoryTimelime();

    public HistoryObject[] GetHistory()
    {
      return _timeline.ToArray();
    }

    public HistoryObject[] GetFocusedHistory(int count)
    {
      return _timeline.GetFocusedHistory(count);
    }

    public void MoveNext()
    {
      if (_timeline.MoveNext())
      {
        Selection.activeObject = _timeline.Current().hObject;
        _lastSelection = _timeline.Current().hObject;

      }
    }

    public void Setup()
    {
      _lastSelection = Selection.activeObject;
    }

    public bool hasNext
    {
      get
      {
        return _timeline.hasNext;
      }
    }

    public bool hasPrevious
    {
      get
      {
        return _timeline.hasPrevious;
      }
    }

    public void MovePrevious()
    {
      if (_timeline.MovePrevious())
      {
        Selection.activeObject = _timeline.Current().hObject;
        _lastSelection = _timeline.Current().hObject;
      }
    }

    public void Update()
    {
      if (_lastSelection != Selection.activeObject)
      {
        if (Selection.activeObject != null)
        {
          _lastSelection = Selection.activeObject;
          _timeline.AddToTimeline(_lastSelection);
        }
      }
    }

    internal void OnElementClicked(HistoryObject obj)
    {
      if (obj != null)
      {
        _timeline.SelectElement(obj);
        _lastSelection = _timeline.Current().hObject;
      }
      Selection.activeObject = _lastSelection;
    }

    public void BeforeSave()
    {
      if(_timeline != null)
      {
        //This should not happen.
        _timeline.BeforeSave();
      }
    }

    public void AfterLoad()
    {
      _timeline.AfterLoad();
    }
  }
}