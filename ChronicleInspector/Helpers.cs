﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChronicleInspector
{
  public class Helpers
  {
    /// <summary>
    /// This function takes a string and tries to make a pretty name to display.
    /// </summary>
    public static string FormatDisplayName(string text, bool preserveAcronyms)
    {
      if (string.IsNullOrEmpty(text))
        return string.Empty;
      StringBuilder newText = new StringBuilder(text.Length * 2);
      newText.Append(char.ToUpper(text[0]));
      for (int i = 1; i < text.Length; i++)
      {
        if (char.IsUpper(text[i]))
          if ((text[i - 1] != '\n' && !char.IsUpper(text[i - 1])) ||
              (preserveAcronyms && char.IsUpper(text[i - 1]) &&
               i < text.Length - 1 && !char.IsUpper(text[i + 1])))
            newText.Append('\n');
        newText.Append(text[i]);
      }
      return newText.ToString();
    }
  }
}
