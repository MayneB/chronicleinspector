﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChronicleInspector.Interfaces
{
  public interface ISave
  {
    void BeforeSave();

    void AfterLoad(); 
  }
}
