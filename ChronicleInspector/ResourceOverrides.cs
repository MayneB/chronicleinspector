﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

public partial class EmbeddedResources
{
  static partial void trackbarThumbStyleOverride(GUIStyle style)
  {
    style.normal.background = trackbarThumb;
    style.fixedWidth = 10;
    style.fixedHeight = 12;
   }

  static partial void breadcrumb_boxStyleOverride(GUIStyle style)
  {
    style.stretchHeight = false;
    style.stretchWidth = true;
    style.border = new RectOffset(3, 3, 5, 5);
    style.padding = new RectOffset(4, 4, 5, 5);
    style.wordWrap = false;
    style.margin = new RectOffset(5, 5, 2, 2);
    style.clipping = TextClipping.Clip;
    style.fixedHeight = 42;
    style.fontSize = 9;
    style.alignment = TextAnchor.UpperCenter;

    style.normal.textColor = Color.grey;
    style.fontStyle = FontStyle.Bold;

    style.active.background = EmbeddedResources.breadcrumb_selected;
  }

  static partial void breadcrumb_primaryStyleOverride(GUIStyle style)
  {
    breadcrumb_boxStyleOverride(style);
  }

}

