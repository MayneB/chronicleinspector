﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChronicleInspector
{
  public static class DataContractConstants
  {
    public const string XML_NAME_HISTORY_TIMELINE = "history_timeline";
    public const string XML_NAME_HISTORY_ARRAY = "history";
    public const string XML_NAME_NEXT_ITEM = "next";
    public const string XML_NAME_PREVIOUS_ITEM = "previous";
    public const string XML_NAME_SELECTION = "primary"; 
    public const string XML_NAME_HISTORY_OBJECT = "history";
    public const string XML_NAME_WATCHER = "watcher";

    //Indexes
    public const string XML_NAME_LOCATION_HEAD = "head";
    public const string XML_NAME_LOCATION_INDEX = "index";
    public const string XML_NAME_LOCATION_TAIL = "tail";

    //History Object
    public const string XML_NAME_HO_ID = "id";
    public const string XML_NAME_HO_IS_SCENE = "is_scene";
  }
}
