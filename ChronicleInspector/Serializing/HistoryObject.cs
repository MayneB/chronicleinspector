﻿using UnityEditor;
using System.Collections.Generic;
using System.Runtime.Serialization;
using uObject = UnityEngine.Object;
using ChronicleInspector.Interfaces;

namespace ChronicleInspector
{
  [DataContract(Name = DataContractConstants.XML_NAME_HISTORY_OBJECT)]
  [System.Serializable]
  public class HistoryObject : ISave
  {
    private const short NoRefernce = -1;

    /// <summary>
    /// Since we can't serialize a UnityObject we
    /// store it's instance id. 
    /// </summary>
    [DataMember(Name=DataContractConstants.XML_NAME_HO_ID)]
    private string _saveID;

    [DataMember(Name=DataContractConstants.XML_NAME_HO_IS_SCENE)]
    private bool _isSceneObject;

    /// <summary>
    /// This is the name of the object that we are going to display.
    /// It's just formatted nicly. 
    /// </summary>
    private string _name; 

    /// <summary>
    /// The Object reference we use in the editor.
    /// </summary>
    [IgnoreDataMember]
    private uObject _hObject;

    public uObject hObject
    {
      get
      {
        return _hObject;
      }
      set
      {
        _hObject = value;

        _name = Helpers.FormatDisplayName(_hObject.name, true);
      }
    }

    public void AfterLoad()
    {
      if(!string.IsNullOrEmpty(_saveID))
      {
        if(!_isSceneObject)
        {
          UnityEngine.Debug.Log("Trying to load");
          hObject = AssetDatabase.LoadAssetAtPath(_saveID, typeof(uObject));

          if(_hObject != null)
          {
            UnityEngine.Debug.Log("Not null and loaded");
          }
        }
        else
        {
          hObject = EditorUtility.InstanceIDToObject(int.Parse(_saveID));
        }
      }
    }

    public void BeforeSave()
    {
      if (_hObject == null)
      {
        _isSceneObject = false;
        _saveID = null;
        return;
      }

      string assetPath = AssetDatabase.GetAssetPath(_hObject);

      if (string.IsNullOrEmpty(assetPath))
      {
        _isSceneObject = true;
        _saveID = _hObject.GetInstanceID().ToString();
      }
      else
      {
        _isSceneObject = false;
        _saveID = assetPath;
      }
    }

    [DataMember(Name = DataContractConstants.XML_NAME_SELECTION)]
    public bool isPrimary = false;

    public string name
    {
      get
      {
        if(_hObject == null)
        {
          return "null";
        }
        else
        {
          return _name;
        }
      }
    }


  }
}