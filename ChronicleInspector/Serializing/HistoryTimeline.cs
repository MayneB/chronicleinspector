﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using uObject = UnityEngine.Object;
using UnityEngine.Serialization;
using System.Runtime.InteropServices;
using ChronicleInspector.Interfaces;


namespace ChronicleInspector
{
  [System.Serializable]
  [DataContract(Name = DataContractConstants.XML_NAME_HISTORY_TIMELINE)]
  public class HistoryTimelime : ISave
  {
    private const int HISTORY_COUNT = 60;

    [DataMember(Name = DataContractConstants.XML_NAME_HISTORY_ARRAY)]
    private HistoryObject[] _history = new HistoryObject[HISTORY_COUNT];
    private HistoryObject[] _tempList = new HistoryObject[HISTORY_COUNT];
    private const int HISTORY_INDEX_INIT_VALUE = -1;

    [DataMember(Name = DataContractConstants.XML_NAME_NEXT_ITEM)]
    private bool _moveNextValid = false;
    [DataMember(Name = DataContractConstants.XML_NAME_PREVIOUS_ITEM)]
    private bool _movePreviousValid = false;

    [DataMember(Name = DataContractConstants.XML_NAME_LOCATION_HEAD)]
    private int _head = 0;

    [DataMember(Name = DataContractConstants.XML_NAME_LOCATION_INDEX)]
    private int _index = HISTORY_INDEX_INIT_VALUE;

    [DataMember(Name = DataContractConstants.XML_NAME_LOCATION_TAIL)]
    private int _tail = 0;

    /// <summary>
    /// This will return true or false based on if
    /// we have a valid target to move forward to. If this 
    /// timeline has empty entries this will also clean it 
    /// up. 
    /// </summary>
    public bool hasNext
    {
      get
      {
        return _moveNextValid;
      }
    }

    /// <summary>
    /// This will return true or false based on if
    /// we have a valid target to move back to. If this 
    /// timeline has empty entries this will also clean it 
    /// up. 
    /// </summary>
    public bool hasPrevious
    {
      get
      {
        return _movePreviousValid;
      }
    }

    public HistoryTimelime()
    {
      _history = new HistoryObject[HISTORY_COUNT];
      _tempList = new HistoryObject[HISTORY_COUNT];
    }

    /// <summary>
    /// The current position of writing head. The next element
    /// will be written here. 
    /// </summary>
    public int head
    {
      get { return _head; }
      set { _head = value; }
    }

    public void DrawDebug()
    {


      return;
      /*
      string message = string.Empty;

      for (int i = 0; i < HISTORY_COUNT; i++)
      {
        if (i == modIndex || i == modHead || i == modTail)
        {
          message += "[";

          if (i == modIndex)
          {
            message += "I";
          }

          if (i == modHead)
          {
            message += "H";
          }

          if (i == modTail)
          {
            message += "T";
          }

          message += "]";
        }
        else if (i == modIndex)
        {
          message += "{I}";
        }
        else if (i == modHead)
        {
          message += "{H}";
        }
        else if (i == modTail)
        {
          message += "{T}";
        }
        else
        {
          message += "*";
        }
      }

      UnityEngine.Debug.Log(message);
       * */
    }

    public HistoryObject Current()
    {
      if (modIndex < 0)
      {
        return null; 
      }

      return _history[modIndex];
    }

    public bool MoveNext()
    {
      Current().isPrimary = false;

      _index++;

      Current().isPrimary = true;

      UpdateMoveGates();

      return _moveNextValid;
    }

    public bool MovePrevious()
    {
      if (_index == _tail)
      {
        return false;
      }

      Current().isPrimary = false;

      _index--;

      Current().isPrimary = true;

      UpdateMoveGates();

      return _movePreviousValid;
    }

    /// <summary>
    /// We use this value to keep our history count below our max. Once
    /// a value goes over the max count we wrap around. 
    /// </summary>
    private int modIndex
    {
      get
      {
        return (_index % HISTORY_COUNT);
      }
    }

    /// <summary>
    /// We use this value to keep our history count below our max. Once
    /// a value goes over the max count we wrap around. 
    /// </summary>
    private int modHead
    {
      get
      {
        return (_head % HISTORY_COUNT);
      }
    }

    public int modTail
    {
      get
      {
        return _tail % HISTORY_COUNT;
      }
    }

    /// <summary>
    /// This ads a target object to the timeline.
    /// </summary>
    /// <param name="entry">The entry you want to add.</param>
    public void AddToTimeline(uObject entry)
    {
      //If this is fresh history we want to set the first element instead
      //of moving ahead. This would cause an out of range error. 
      if (_index == HISTORY_INDEX_INIT_VALUE)
      {
        _index = 0;
        _history[_index] = new HistoryObject() { hObject = entry, isPrimary = true };
        UpdateMoveGates();
        return;
      }

      if (Current() != null)
      {
        Current().isPrimary = false;
      }

      _head++;

      if (_index == _head)
      {
        _index = _head;
        _history[modIndex] = new HistoryObject() { hObject = entry };
      }
      else
      {
        _index++;
        _head = _index;
        _history[modIndex] = new HistoryObject() { hObject = entry };
      }

      if (_index >= modTail + HISTORY_COUNT)
      {
        //If we have done a full wrap around we have to increse our tail.  
        _tail++;
      }

      UpdateMoveGates();

      Current().isPrimary = true;

      DrawDebug();
    }

    /// <summary>
    /// The "move gates" are just the name for the tip and tail indexes.
    /// This function makes sure they down overlap each other. 
    /// </summary>
    private void UpdateMoveGates()
    {
      DrawDebug();
      _movePreviousValid = _index > _tail;

      _moveNextValid = _index < _head;
    }

    /// <summary>
    /// When a list starts to get out of order we have a bunch of null
    /// objects. This function will clear them up whenever
    /// Next or Previous finds a null entry. 
    /// </summary>
    private void RemoveEmptyEntries()
    {

      int newHead = -1;
      int current = -1;

      if(_tempList == null)
      {
        _tempList = new HistoryObject[HISTORY_COUNT];
      }

      if(Current() != null)
      {
        Current().isPrimary = false;
      }

      //We now have the length of our trimed list [newHead] now we just have to update our currentList;
      for (int i = _tail; i < _head + 1; i++ )
      {
        if (_history[modValue(i)] != null && _history[modValue(i)].hObject != null)
        {
          if (_history[modValue(i)] == Current() || current != -1)
          {
            UnityEngine.Debug.Log("Found head: " + i);
            current = newHead;
          }
          newHead++;

          //Since we are in range we update our list. 
          _tempList[newHead] = _history[modValue(i)];
        }
      }

      _history = _tempList;

      //And reset our selections
      _index = current;
      _head = newHead;
      _tail = 0;

      if(Current() != null)
      {
        Current().isPrimary = true;
      }
    }

    public HistoryObject this[int index]
    {
      get
      {
        return _history[index];
      }
      set
      {
        _history[index] = value;
      }
    }

    public HistoryObject[] ToArray()
    {
      return _history;
    }

    public void Reset()
    {
      _head = 0;
      _index = HISTORY_INDEX_INIT_VALUE;
      _tail = 0;
      DrawDebug();
    }

    internal HistoryObject[] GetFocusedHistory(int count)
    {
      List<HistoryObject> tempArray = new List<HistoryObject>();

      if (_index == HISTORY_INDEX_INIT_VALUE)
      {
        return tempArray.ToArray();
      }

      for (int i = -count; i < count + 1; i++)
      {
        int tempIndex = _index + i;
        int tempMod = modValue(tempIndex);

        //This checks to make sure we are on the 
        //positive side ie _index ++. At this point 
        //we are only looking foward in histroy
        if (i > 0)
        {
          if (tempIndex > _head)
          {
            continue;
          }
        }
        //Now we only check stuff looking forward. 
        else if (i < 0)
        {
          if (tempIndex < _tail)
          {
            continue;
          }
        }

        tempArray.Add(_history[tempMod]);
      }

      return tempArray.ToArray();
    }

    private int modValue(int value)
    {
      return value % HISTORY_COUNT;
    }

    /// <summary>
    /// When a user clicks on an entrie this function is used to select it.
    /// This is harder then it looks because we don't store entries in a normal
    /// list. They are stored on a static one using Indexes. 
    /// </summary>
    /// <param name="obj"></param>
    internal void SelectElement(HistoryObject obj)
    {
      RemoveEmptyEntries();

      if(Current() != null)
      {
        Current().isPrimary = false;
      }

      for (int i = _head; i != _tail - 1; i-- )
      {
        if(_history[modValue(i)] == obj)
        {
          _index = i;
          break;
        }
      }

      if(Current() != null)
      {
        Current().isPrimary = true;
      }

      UpdateMoveGates();
    }

    /// <summary>
    /// This will go over each timeline element
    /// and save their data to the class. This way
    /// we know our serializer will write the correct data. 
    /// </summary>
    public void BeforeSave()
    {
      for (int i = 0; i < _history.Length; i++)
      {
        if (_history[i] != null)
        {
          _history[i].BeforeSave();
        }
      }
    }

    /// <summary>
    /// After the classes are loaded we need to make sure
    /// that we take the save data and load the objects. 
    /// </summary>
    public void AfterLoad()
    {
      for (int i = 0; i < _history.Length; i++)
      {
        if (_history[i] != null)
        {
          _history[i].AfterLoad();
        }
      }

      //The temp list is not serialized so we have to reset it on load. 
      _tempList = new HistoryObject[HISTORY_COUNT]; 

      //Now we have to remove our empty entries
      RemoveEmptyEntries(); 
    }
  }
}
