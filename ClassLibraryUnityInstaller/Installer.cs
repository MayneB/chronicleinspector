﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Linq;

namespace ClassLibraryUnityInstaller
{
  [InitializeOnLoad]
  public class Installer : ScriptableObject
  {
    static Installer()
    {
      InstallPackage();

    }

    private void OnEnable()
    {
      this.hideFlags = HideFlags.DontSave;
    }

    private static void InstallPackage()
    {
      Directory.CreateDirectory(rootExtensionPath);

      string chronicleLocalDLLPath = FindSystemPackagepathByname("t:Script ChronicleEditor");

      if (string.IsNullOrEmpty(chronicleLocalDLLPath))
      {
        //The asset has been moved or deleted
        string installerPath = FindSystemPackagepathByname("ChronicleClassLibraryUnityInstaller");
        File.Delete(installerPath);
        AssetDatabase.Refresh();
        return;
      }

      EditorUtility.RevealInFinder(EditorApplication.applicationContentsPath);

      EditorUtility.RevealInFinder(Application.dataPath + "/UnityExtensions/");

      EditorUtility.DisplayDialog("How to Install Chronicle", "Because I can't grab the proper securty premissions I have to request the user to install the package. I have opened two windows. Please Drag 'UnityExtensions' into the 'Data' folder", "I Understand");
    }

    private static string FindSystemPackagepathByname(string name)
    {
      string guid = AssetDatabase.FindAssets(name).FirstOrDefault();

      if (string.IsNullOrEmpty(guid))
      {
        //The package is missing or it's been moved.
        return null;
      }

      string path = AssetDatabase.GUIDToAssetPath(guid);

      if (string.IsNullOrEmpty(path))
      {
        return null;
      }

      path = Application.dataPath.Replace("Assets", string.Empty) + path;

      return path;
    }

    private const string m_Auther = "BMayne";
    private const string m_ExtensionName = "Chronicle";
    private const string m_Version = "1.0.1";

    private static string rootExtensionPath
    {
      get
      {
        return string.Format("{0}/UnityExtensions/{1}/{2}/{3}/",
            EditorApplication.applicationContentsPath,
            m_Auther,
            m_ExtensionName,
            m_Version);
      }
    }

    public static string dllPath
    {
      get
      {
        return rootExtensionPath + "/Chronicle.dll";
      }
    }

    public static string ivyPath
    {
      get
      {
        return rootExtensionPath + "/ivy.xml";
      }
    }

  }
}
