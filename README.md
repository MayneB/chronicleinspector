# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Chronicle Inspector is a simple to use history based object inspector for Unity. It allows you to go back and forth between selections with easy. It also has some other built in features like Unity's Debug Internal.
* 1.0.3

### How do I get set up? ###

* Summary of set up
This project is a Unity dll that sits in the root of your Unity extensions folder. Once you build this project take the following step below to get working.

      Windows: Copy the contents of Output folder to: 


```
#!path

Data\UnityExtensions\BMayne\Chronicle\{UNITY_VERSION}
```


      OSX: Copy the contents of Output folder to:


```
#!path

Unity.app/Contents/UnityExtensions/BMayne/Chronicle/{UNITY_VERSION}

```
While you look over this code you might notice there are some over complications of some ideas. This was done on purpose. This tool was created just as a learning experience. I wanted to try to learn new concepts that I have not had experience with before. The best examples of this are the DataContactSerializer and the T4 resource generator. 

*
### Contribution guidelines ###

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact